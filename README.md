# Taller: Predicci�n de series temporales usando deep learning

En este repositiorio se encuentra disponible todo el material necesario para poner en pr�ctica lo provisto en el [taller](http://santiago.bigdataweek.com/session/taller-prediccion-de-series-temporales-usando-deep-learning/).

El repositorio cuenta con un Dockerfile que permite replicar el ambiente de python con las librer�as necesarias, adem�s de contar con el dataset que se utiliz� en el taller.

El jupyter notebook contiene el paso a paso de lo revisado y ejecutado durante el taller.

Como referencias adicionales sobre el tema de series temporales y deep learning, se pueden revisar los siguientes links:

* [Blog que explica de gran forma las redes neuronales recurrentes](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

* [Blog que contiene una gran variedad de tutoriales y gu�as para aplicar ML a series temporales](https://machinelearningmastery.com/)

* [Informaci�n del dataset](https://archive.ics.uci.edu/ml/datasets/individual+household+electric+power+consumption)

* [Documentaci�n de Keras](https://keras.io/)

Para m�s detalles, por favor contactar a Joaqu�n Ureta al email joaquin@moebius-analitica.com o juakonap@gmail.com
